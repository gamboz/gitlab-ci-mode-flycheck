;;; gitlab-ci-mode-flycheck.el --- Flycheck support for ‘gitlab-ci-mode’ -*- lexical-binding: t; -*-
;;
;; Copyright 2018 Joe Wreschnig
;;
;; Author: Joe Wreschnig
;; Keywords: tools, vc, convenience
;; Package-Commit: eba81cfb7224fd1fa4e4da90d11729cc7ea12f72
;; Package-Requires: ((emacs "25") (flycheck "31") (gitlab-ci-mode "1"))
;; Package-Version: 20190323.1829
;; Package-X-Original-Version: 20180605.1
;; URL: https://gitlab.com/joewreschnig/gitlab-ci-mode-flycheck/
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:
;;
;; Flycheck integration for the linter included with ‘gitlab-ci-mode’.
;; For security reasons this checker is not enabled by default, as the
;; linter works by sending your GitLab CI configuration to a remote
;; server.  To enable it, call ‘gitlab-ci-mode-flycheck-enable’.


;;; Code:

(require 'flycheck)
(require 'gitlab-ci-mode)

(defun gitlab-ci-mode-flycheck--goto-path (path)
  "Go to the YAML key described by a “:”-separated PATH string.

If the full path could not be resolved, go to the last element
which could be found."
  (goto-char 0)
  (condition-case nil
      (let ((prefix "^"))
        (dolist (key (split-string path ":"))
          (re-search-forward
           (concat "\\(" prefix "\\)\\<\\(" key "\\)\\> *:"))
          (setq prefix (concat (match-string 1) " +"))
          (goto-char (match-end 2))))
    (search-failed nil)))

(defun gitlab-ci-mode-flycheck--line-for-message (message)
  "Try figure out the line number described by MESSAGE.

If the full key in the message could not be found, attribute the
error to the last element which could be found."
  (cond ((string-match
          "\\(?:jobs:\\)?\\([^ ]+\\) .* keys: \\([^ ]+\\)" message)
         (gitlab-ci-mode-flycheck--goto-path
          (concat (match-string 1 message) ":" (match-string 2 message))))
        ((string-match "\\(?:jobs:\\)?\\([^ ]+\\) config" message)
         (gitlab-ci-mode-flycheck--goto-path (match-string 1 message)))
        ((string-match "jobs:\\([^ ]+\\)" message)
         (gitlab-ci-mode-flycheck--goto-path (match-string 1 message)))
        (t (goto-char 0)))
  (line-number-at-pos))

(defun gitlab-ci-mode-flycheck--errors-filter (errors)
  "Fix up the line numbers of each error in `errors', if necessary."
  (dolist (err errors)
    (when (and (not (flycheck-error-line err)) (flycheck-error-message err))
      (flycheck-error-with-buffer err
        (save-restriction
          (save-mark-and-excursion
           (widen)
           (gitlab-ci-mode-flycheck--line-for-message
            (flycheck-error-message err))
           (setf (flycheck-error-line err) (line-number-at-pos)
                 (flycheck-error-column err) (current-column)))))))
  errors)

(defun gitlab-ci-mode-flycheck--errors-parser (checker syntax-errors)
  "Parse errors received from gitlab's ci/cd yaml linter
into something that Flycheck understands.

This function will end up as :callback to (glab-post).
"
  (mapcar
   (lambda (error-message)
     (flycheck-error-new-at
      (when (string-match "\\<line \\([0-9]+\\)\\>" error-message)
        (string-to-number (match-string 1 error-message)))
      (when (string-match "\\<column \\([0-9]+\\)\\>" error-message)
        (string-to-number (match-string 1 error-message)))
      'error error-message :checker checker))
   syntax-errors))


(defun gitlab-ci-mode-checker-start  (checker status-change-callback)
  "A function to start the syntax checker.

The first argument is the syntax checker being started.

The second is a callback function to report state changes to
Flycheck.  The callback takes two arguments STATUS DATA, where
STATUS is a symbol denoting the syntax checker status and DATA an
optional argument with additional data for the status report.

"
  (defun gitlab-ci-mode-flycheck--ghub-wrapper (data headers status ghub--req-struct)
  "This function can be given to (gitlab-ci-request-lint) as callback.

Such a callback is passed along as :callback to (glab-post) and
thus needs to have the above signature.

Also, here we transform the syntax errors reported by gitlab-ci
lint into flycheck errors, and for that we need to know the
`checker'.

Finally I'm defining a function instead of using a lambda (or
macro?) because I'm stupid ;)
"
  ;;TODO: manage errors
  (gitlab-ci-mode-flycheck--errors-parser
   checker
   (alist-get 'errors data))
  )

  (condition-case err
      ;; TODO write a func to parse the errors and replace ...lint-to-buffer
      (let ((syntax-errors
             (gitlab-ci-request-lint 'gitlab-ci-mode-flycheck--ghub-wrapper)))
        (funcall status-change-callback 'finished syntax-errors))
    (message (error-message-string err)))
  )

;; (lambda (status data)
;;   (if (eq status 'errored)
;;     (funcall
;;      callback ;; expects (data headers status ghub--req-struct)
;;      (mapcar
;;       (lambda (message)
;;         (flycheck-error-new-at
;;          (when (string-match "\\<line \\([0-9]+\\)\\>" message)
;;            (string-to-number (match-string 1 message)))
;;          (when (string-match "\\<column \\([0-9]+\\)\\>" message)
;;            (string-to-number (match-string 1 message)))
;;          'error message :checker checker))
;;       (alist-get 'errors data))
;;      nil
;;      status
;;      nil
;;      )))
;; :silent))

(flycheck-define-generic-checker 'gitlab-ci
  "Lint GitLab CI configuration files.

This checker will send your file to a remote service, as GitLab
offers no local linting tool. The service URL is configurable via
‘gitlab-ci-url’.

Because the GitLab CI linter does not give line numbers for most
errors, line-level attribution may be incorrect when using some
YAML features such as references, tags, or unusual indentation."
  :start #'gitlab-ci-mode-checker-start

  :error-filter #'gitlab-ci-mode-flycheck--errors-filter

  :modes '(gitlab-ci-mode))

;;;###autoload
(defun gitlab-ci-mode-flycheck-enable ()
  "Enable Flycheck support for ‘gitlab-ci-mode’.

Enabling this checker will upload your buffer to the site
specified in ‘gitlab-ci-url’.  If your buffer contains sensitive
data, this is not recommended.  (Storing sensitive data in your
CI configuration file is also not recommended.)

If your GitLab API requires a private token, set
‘gitlab-ci-api-token’."
  (add-to-list 'flycheck-checkers 'gitlab-ci))


(provide 'gitlab-ci-mode-flycheck)
;;; gitlab-ci-mode-flycheck.el ends here
